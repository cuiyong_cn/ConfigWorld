-- Show line number
vim.o.number = true
-- Width of the tab
vim.o.tabstop = 4
vim.o.shiftwidth = 4
-- Ignore case when search
vim.o.smartcase = true
vim.o.ignorecase = true
-- Colorscheme
vim.cmd.colorscheme('retrobox')

-- Keymaps
vim.keymap.set('i', 'jk', '<esc>', { silent = true, noremap = true })
