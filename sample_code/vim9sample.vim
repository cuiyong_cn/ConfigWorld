vim9script

# Comments start with '#', and extend to the end of line
# First line must be vim9script command

# vim9 basics
# Declare variable with 'var'
# The type of variable is inferred from the value
var i = 10           # integer
var f = 10.0         # float
var s = 'abc'        # string
var a = [1, 2, 3]    # list
var d = {a: 1, b: 2} # dictionary
var b = true         # boolean

# Control structures
if i > 0
    echo 'i is positive'
elseif i < 0
    echo 'i is negative'
else
    echo 'i is zero'
endif

# Loops, and variable in function args can not be shadowed
for num in range(10)
    echo num
endfor

while i > 0
    i -= 1
    echo i
endwhile

# Function can be defined with 'def', 'enddef', and must start with capital letter
def Square(x: number): number
    return x * x
enddef

echo Square(2) # 4

# We can get a funcref by 'funcref', and variable must start with capital letter
var SqFunc = funcref('Square')
echo SqFunc(2) # 4

import './math.vim'

echo math.Add(50, 2) # 52