# Check soname of a shared library.
objdump -p <lib.so> | grep SONAME
readelf -d <lib.so> | grep SONAME

# Check shared libs a process is currently using.
cat /proc/<pid>/maps

# Shared library real name, soname and link name.
real name: libfoo.so.<major>.<minor>.<patch>
soname: libfoo.so.<major>
linkname: libfoo.so

Why do we do such thing?
- Version management
- Compatibility management
- Convenient for development
