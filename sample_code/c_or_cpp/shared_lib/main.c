#include <stdio.h>

/**
 * Both main executable and shared lib defined hello.
 * But still shared lib uses the main's hello not its own's definition.
 * Why does it behave like this?
 *   Historical reason:
 *     The first shared library implementations were designed so that the default semantics
 *     for symbol resolution exactly mirrored those of applications linked against static
 *     equivalents of the same libraries.
 *   This means:
 *     - A definition of a global symbol in the main program overrides a definition in a library.
 *     - If a global symbol is defined in multiple libraries, then a reference to that symbol
 *       is bound to the first definition found by scanning libraries in the left-to-right order
 *       in which they were listed on the static link command line.
 *
 * Now the question is: can we make the lib use its own definition?
 * The answer is yes: by using -Bsymbolic linker option.
 */
void hello()
{
    printf("Hello from main.");
}

void say_hello();

int main()
{
    say_hello();

    return 0;
}
