use std::ops::Deref;
use std::thread;
use std::sync::{Arc, Mutex};

//! This is module-level documentation, you can put doc tests here
//!
//! # Examples
//! ```
//! assert!(true);
//! ```

/// This is function-level documentation, doc tests are also allowed here.
///
/// # Examples
/// ```
/// assert!(true)
/// ```
struct MyBox<T: std::fmt::Display> (T);

impl<T: std::fmt::Display> MyBox<T> {
    fn new(val: T) -> MyBox<T> {
        MyBox(val)
    }
}

impl<T: std::fmt::Display> Deref for MyBox<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: std::fmt::Display> Drop for MyBox<T> {
    fn drop(&mut self) {
        println!("Droped {}", &self.0);
    }
}

/// We can use features section in Cargo.toml to do conditional compilation.
/// You can pass these feature flag through cargo build --featurs "64bit"
///
#[cfg(arch = "64bit")]
mod arch64 {
    pub fn os_type() -> String {
        // cfg! can be used in code
        if cfg!(target_os = "macos") || cfg!(target_os = "ios") {
            "Apple OS"
        } else {
            "unknown"
        }
    }
}

fn main() {
    {
        let x = MyBox::new(5);

        assert_eq!(5, *x);
    }

    let v = vec![1, 2, 3];
    let handle = thread::spawn(move || {
        println!("Here is a vector: {v:?}");
    });

    handle.join().unwrap();

    println!("outside");
}

pub trait Messenger {
    fn send(&self, msg: &str);
}

pub struct LimitTracker<'a, T: Messenger> {
    messenger: &'a T,
    value: usize,
    max: usize,
}

impl<'a, T> LimitTracker<'a, T>
where
    T: Messenger,
{
    pub fn new(messenger: &'a T, max: usize) -> LimitTracker<'a, T> {
        LimitTracker {
            messenger,
            value: 0,
            max,
        }
    }

    pub fn set_value(&mut self, value: usize) {
        self.value = value;

        let percentage_of_max = self.value as f64 / self.max as f64;

        if percentage_of_max >= 1.0 {
            self.messenger.send("Error: You are over your quota!");
        } else if percentage_of_max >= 0.9 {
            self.messenger.send("Urgent warning: You've used up over 90% of your quota!");
        } else if percentage_of_max >= 0.75 {
            self.messenger.send("Warning: You've used up over 75% of your quota!");
        }
    }
}

fn mutexTest() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });

        handles.push(handle);
    }

    for handle in handles {
        handle.join().unwrap();
    }

    println!("Result: {}", *counter.lock().unwrap());
}

/// We can put tests directly inside the source code
#[cfg(test)]
mod tests {
    use super::*;
    use std::cell::RefCell;

    struct MockMessenger {
        sent_messages: RefCell<Vec<String>>,
    }

    impl MockMessenger {
        fn new() -> MockMessenger {
            MockMessenger {
                sent_messages: RefCell::new(vec![]),
            }
        }
    }

    impl Messenger for MockMessenger {
        fn send(&self, message: &str) {
            self.sent_messages.borrow_mut().push(String::from(message));
        }
    }

    #[test]
    fn it_send_an_over_75_percent_warning_message() {
        let mock_messenger = MockMessenger::new();
        let mut limit_tracker = LimitTracker::new(&mock_messenger, 100);

        limit_tracker.set_value(80);

        assert_eq!(mock_messenger.sent_messages.borrow().len(), 1);
    }
}
