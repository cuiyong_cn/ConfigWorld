#!/usr/bin/env lua
-- Comments:
--     Lua sample code.
--
-- table
days = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" }

-- standalone script's arguments are stored in global table arg
local script_name = arg[0]
local arg1 = arg[1]
local arg2 = arg[2]

-- range
for i = 1, 7 do
    print(days[i])
end

for k, v in pairs(days) do
    print(k, v)
end

-- safe navigation emulation
E = {}
zip = (((company or E).director or E).address or E).zipcode or {}

-- Shuffle a list
function shuffle(x)
    math.randomseed(os.time())
    for i = #x, 2, -1 do
        local j = math.random(i)
        x[i], x[j] = x[j], x[i]
    end

    return x
end

function permgen(a, n)
    if 0 == n then
        coroutine.yield(a)
    else
        for i = 1,n do
            -- put ith element as the last one
            a[n], a[i] = a[i], a[n]
            -- gen all pemutations of the other elements
            permgen(a, n - 1)
            -- restore ith element
            a[n], a[i] = a[i], a[n]
        end
    end
end

function perm(a)
    local n = #a -- old style: table.getn(a)
    return coroutine.wrap(function() permgen(a, n) end)
end

function printArray(a)
    for i, v in ipairs(a) do
        io.write(v, " ")
    end
    io.write("\n")
end

function get_file_size(f)
    local cur = f:seek()
    local size = f:seek("end")
    f:seek("set", cur)
    return size
end

function get_last_line(f)
    local cur = f:seek();

    for i = 2, math.huge do
        local pos = f:seek("end", -i)
        if nil == pos then
            f:seek("set")
            break
        end

        local c = f:read(1)
        if "\n" == c  then
            break;
        end
    end

    local line = f:read("*l")

    f:seek("set", cur)

    return line
end

function time_cost(func)
    local t_start = os.clock()
    func()
    local t_end = os.clock()
    io.write("Total time: ", t_end - t_start, "\n")
end

function file_exists(path)
    local f = io.open(path, "r")
    return f ~= nil and f:close()
end
