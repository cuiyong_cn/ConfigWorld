{-
 - Factorial in Haskell using pattern match
-}
fac :: Int -> Int
fac 0 = 1
fac n = n * fac(n - 1)

-- Haskell's entry point is also main
main = do
    print "Hello Haskell"
    print (fac(5))
