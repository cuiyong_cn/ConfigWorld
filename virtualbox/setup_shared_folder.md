# Create a shared folder

In virtual box settings, create a shared folder

# Install vbox additions

sudo apt-get install virtualbox-guest-x11

# mount the shared folder

sudo mount -t vboxsf -o `shared_name` `folder_name`
