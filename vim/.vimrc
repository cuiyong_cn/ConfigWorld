" VIM UI Setting {{{
set number
set ruler
set showcmd
set noshowmode

" airline is a pretty new way to show status line
set laststatus=2
set wildmenu
" Change the language to English in non-english system
set langmenu=en_US
let $LANG='en_US'
"source $VIMRUNTIME/delmenu.vim
"source $VIMRUNTIME/menu.vim

" Long line column marker
"highlight ColorColumn term=standout ctermbg=12 guibg=Red
set textwidth=100
set colorcolumn=+1
colorscheme darkblue

set listchars=tab:>-,trail:-
" }}}

" Mouse Settings {{{
if has('mouse')
    set mouse=a
endif
" }}}

" Encoding Settings {{{
set encoding=utf-8
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1
" }}}

" line ending Settings {{{
set fileformats=unix,dos,mac
" }}}

" Indenting Settings {{{
set expandtab
set smartindent
set smarttab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set backspace=start,indent,eol
set cinoptions+=:0
" }}}

" Search Setting {{{
set incsearch
if &t_Co > 2 || has("gui_running")
	syntax on
	set hlsearch
endif
set ignorecase
set smartcase

filetype plugin indent on
" }}}

" Fold Settings {{{
set foldcolumn=3
set foldmethod=indent
" }}}

" Plugin Settings {{{
filetype plugin on
" }}}

" Abbrevation Defining {{{
iabbrev adn and
iabbrev tehn then
iabbrev teh the
iabbrev waht what
iabbrev taht that
iabbrev <expr> dts strftime('%c')
" }}}

" GTags settings {{{
set tags=./.tags;,.tags
" Remember to install pygments -- pip install pygments
let $GTAGSLABEL = 'native-pygments'

if has('win32')
    let $GTAGSCONF = '\path\to\gtags\share\gtags\gtags.conf'
elseif has('win32unix')
    " cygwin does not has gtags, you have to install it by yourself
    let $GTAGSCONF = '/usr/local/share/gtags/gtags.conf'
else
    let $GTAGSCONF = '/etc/gtags/gtags.conf'
endif
" }}}

" gutentags settings {{{
"set statusline+=%{gutentags#statusline('[',']')}
" debug enable
let g:gutentags_trace=0
let g:gutentags_project_root = ['.root', '.git' ]
let g:gutentags_ctags_tagfile = '.tags'
let g:gutentags_generate_on_write = 0

let g:gutentags_modules = []
if executable('ctags')
    let g:gutentags_modules += ['ctags']
endif

if executable('gtags-cscope') && executable('gtags')
    let g:gutentags_modules += ['gtags_cscope']
endif

let g:gutentags_background_update = 0

let s:vim_tags = expand('~/.vim/cache/tags')
let g:gutentags_cache_dir = s:vim_tags
" --extras only supported by universal-ctags, exuberant-ctags support --extra
let g:gutentags_ctags_extra_args = ["--fields=+niazS", "--extras=+q"]
let g:gutentags_ctags_extra_args += ["--c++-kinds=+px"]
let g:gutentags_ctags_extra_args += ["--c-kinds=+px"]
" Enable the line below if you use universal-ctags
let g:gutentags_ctags_extra_args += ["--output-format=e-ctags"]
if !isdirectory(s:vim_tags)
    silent! call mkdir(s:vim_tags, 'p')
endif

" Disable the gutentaga automatically loading the gtags database
" This is accomplished by gutentags_plus plugin
let g:gutentags_auto_add_gtags_cscope = 0
" }}}

" Gutentags Plus Settings {{{
let g:gutentags_plus_nomap = 1
noremap <silent> <leader>gs :GscopeFind s <C-R><C-W><cr>
noremap <silent> <leader>gg :GscopeFind g <C-R><C-W><cr>
noremap <silent> <leader>gc :GscopeFind c <C-R><C-W><cr>
noremap <silent> <leader>gt :GscopeFind t <C-R><C-W><cr>
noremap <silent> <leader>ge :GscopeFind e <C-R><C-W><cr>
noremap <silent> <leader>gf :GscopeFind f <C-R>=expand("<cfile>")<cr><cr>
noremap <silent> <leader>gi :GscopeFind i <C-R>=expand("<cfile>")<cr><cr>
noremap <silent> <leader>gd :GscopeFind d <C-R><C-W><cr>
noremap <silent> <leader>ga :GscopeFind a <C-R><C-W><cr>
" }}}

" asyncrun settings {{{
let g:asyncrun_open = 6
let g:asyncrun_bell = 1
let g:asyncrun_timer = 1000
let g:asyncrun_rootmarks = ['.svn', '.git', '.root', '_darcs', 'build.xml']

nnoremap <silent> <F4> :AsyncRun -cwd=<root> cmake . <cr>
nnoremap <silent> <F5> :AsyncRun -raw -cwd=$(VIM_FILEDIR) -mode=4 "$(VIM_FILEDIR)/$(VIM_FILENOEXT)" <cr>
nnoremap <silent> <F6> :AsyncRun -cwd=<root> -raw make clean<cr>
nnoremap <silent> <F7> :AsyncRun -cwd=<root> make <cr>
nnoremap <silent> <F8> :AsyncRun -cwd=<root> -raw -mode=4 make run <cr>
nnoremap <silent> <F9> :AsyncRun gcc -Wall -O2 "$(VIM_FILEPATH)" -o "$(VIM_FILEDIR)/$(VIM_FILENOEXT)" <cr>
nnoremap <F10> :call asyncrun#quickfix_toggle(6)<cr>
" }}}

" ale settings {{{
let g:ale_linters_explicit = 1
let g:ale_completion_delay = 500
let g:ale_echo_delay = 20
let g:ale_lint_delay = 500
let g:ale_echo_msg_format = '[%linter%] %code: %%s'
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:airline#extensions#ale#enabled = 1

let g:ale_c_gcc_options = '-Wall -O2 -std=c99'
let g:ale_cpp_gcc_options = '-Wall -O2 -std=c++14'
let g:ale_c_cppcheck_options = ''
let g:ale_cpp_cppcheck_options = ''
let g:ale_sign_error = "\ue009\ue009"

hi! clear SpellBad
hi! clear SpellCap
hi! clear SpellRare
hi! SpellBad gui=undercurl guisp=red
hi! SpellCap gui=undercurl guisp=blue
hi! SpellRare gui=undercurl guisp=magenta
" }}}

" ycm settings {{{
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_show_diagnostics_ui = 0
let g:ycm_server_log_level = 'info'
let g:ycm_min_num_identifier_candidate_chars = 2
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_complete_in_strings=1
let g:ycm_clangd_binary_path='~/.vim/pack/ide/start/YouCompleteMe/third_party/ycmd/third_party/clangd/output/bin/clangd'
set completeopt=menu,menuone
let g:ycm_semantic_triggers =  {
           \ 'c,cpp,python,java,go,erlang,perl': ['re!\w{2}'],
           \ 'cs,lua,javascript': ['re!\w{2}'],
           \ }
if has('win32')
    let g:ycm_global_ycm_extra_conf = '~/vimfiles/cache/YouCompleteMe/.ycm_extra_conf.py'
else
    let g:ycm_global_ycm_extra_conf = '~/.vim/cache/YouCompleteMe/.ycm_extra_conf.py'
endif

" }}}

" light-bufferline Settings {{{
nmap <Leader>1 <Plug>lightline#bufferline#go(1)
nmap <Leader>2 <Plug>lightline#bufferline#go(2)
nmap <Leader>3 <Plug>lightline#bufferline#go(3)
nmap <Leader>4 <Plug>lightline#bufferline#go(4)
nmap <Leader>5 <Plug>lightline#bufferline#go(5)
nmap <Leader>6 <Plug>lightline#bufferline#go(6)
nmap <Leader>7 <Plug>lightline#bufferline#go(7)
nmap <Leader>8 <Plug>lightline#bufferline#go(8)
nmap <Leader>9 <Plug>lightline#bufferline#go(9)
nmap <Leader>0 <Plug>lightline#bufferline#go(10)

nmap <Leader>c1 <Plug>lightline#bufferline#delete(1)
nmap <Leader>c2 <Plug>lightline#bufferline#delete(2)
nmap <Leader>c3 <Plug>lightline#bufferline#delete(3)
nmap <Leader>c4 <Plug>lightline#bufferline#delete(4)
nmap <Leader>c5 <Plug>lightline#bufferline#delete(5)
nmap <Leader>c6 <Plug>lightline#bufferline#delete(6)
nmap <Leader>c7 <Plug>lightline#bufferline#delete(7)
nmap <Leader>c8 <Plug>lightline#bufferline#delete(8)
nmap <Leader>c9 <Plug>lightline#bufferline#delete(9)
nmap <Leader>c0 <Plug>lightline#bufferline#delete(10)
" }}}

" LeaderF settings {{{
let g:Lf_ShortcutF = '<c-p>'
if has('win32') || has('win32unix')
    let g:Lf_ShortcutB = '<m-n>'
    noremap <leader>f :LeaderfMru<cr>
    noremap <leader>b :LeaderfBuffer<cr>
    noremap <leader>t :LeaderfTag<cr>
else
    let g:Lf_ShortcutB = 'î'
    noremap æ :LeaderfMru<cr>
    noremap â :LeaderfBuffer<cr>
    noremap ô :LeaderfTag<cr>
endif

let g:Lf_StlSeparator = { 'left': '', 'right': '', 'font': '' }

let g:Lf_RootMarkers = ['.project', '.root', '.svn', '.git']
let g:Lf_WorkingDirectoryMode = 'Ac'
let g:Lf_WindowHeight = 0.30
let g:Lf_CacheDirectory = expand('~/.vim/cache')
let g:Lf_ShowRelativePath = 0
let g:Lf_HideHelp = 1
let g:Lf_StlColorscheme = 'powerline'
let g:Lf_PreviewResult = {'Function':0, 'BufTag':0}
" }}}

" Taglist Settings {{{
let g:Tlist_Sort_Type = 'name'
let g:Tlist_Use_Right_Window = 1
" }}}

" Autocmd Defining {{{
" vimscript file setting {{{
augroup filetype_vim
	autocmd!
	autocmd FileType vim setlocal foldmethod=marker
augroup END
" }}}

autocmd BufReadPost *
	\ if line("'\"") > 1 && line("'\"") < line("$") |
	\   exe "normal! g`\"" |
	\ endif

augroup filetype_html
	autocmd!
	autocmd FileType html nnoremap <buffer> <localleader>f Vatzf
	"autocmd BufRead,BufWritePre *.html :normal gg=G
augroup END

" }}}

" Keymap Defining {{{
nnoremap <f3> :PreviewTag<cr>
nnoremap <leader>ev :vs $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <leader>g :silent AsyncRun 'rg -H -n ' . shellescape(expand('<cWORD>'), '4')

inoremap jk <esc>
" Upper the case of the word under the cursor
inoremap <C-c> <esc>mzgUiw`za

onoremap p i{
onoremap in( :<c-u>normal! f(vi(<cr>
onoremap il( :<c-u>normal! F)vi(<cr>
onoremap in{ :<c-u>normal! f{vi{<cr>
onoremap il{ :<c-u>normal! F}vi{<cr>
"onoremap ih  :<c-u>execute "normal! ?^==\\+$\r:nohlsearch\rkvg_"<cr>
" }}}

" Env Path settings {{{
if has('win32')
    let $PATH .= ";" . $HOME . '\vimfiles\bin'
else
    echo ""
    let $PATH .= ":" . $HOME . '/.vim/bin'
endif
" }}}

highlight ColorColumn ctermbg=red

let g:FavColorschemes = ["darkblue", "zellner", "blue", "evening"]

function SetColorSchemeBasedOnTimeOfDay()
    let idx = strftime("%H") % len(g:FavColorschemes)
    if g:colors_name !~ g:FavColorschemes[idx]
        execute "colorscheme " . g:FavColorschemes[idx]
    endif
endfunction

"let g:lightline = {
"      \ 'colorscheme': 'wombat',
"      \ 'active': {
"      \   'left': [ [ 'mode', 'paste' ],
"      \             [ 'readonly', 'filename', 'modified', 'TimeOfDayColorscheme' ] ]
"      \ },
"      \ 'component': {
"      \   'TimeOfDayColorscheme': '%{SetColorSchemeBasedOnTimeOfDay()}'
"      \ },
"      \ }

function! DeleteTrailingSpaces()
    execute "%s/\\s\\+$//"
endfunction
