vim9script noclear

if exists('g:loaded_my_template')
    finish
endif

g:loaded_my_template = 1

augroup MyTemplate
    autocmd!
    autocmd BufNewFile CMakeLists.txt :0r ~/.vim/templates/CMakeLists.txt
augroup END
