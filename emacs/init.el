(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq inferior-lisp-program "clisp")
(global-display-line-numbers-mode)
(add-hook 'text-mode-hook 'auto-fill-mode)
(setq-default fill-column 80)
(setq backup-inhibited t)

; Emacs UI Settings
(menu-bar-mode -1)

; Emacs CC Mode Settings
(setq c-basic-offset 4)
(setq c-default-style '((java-mode . "java")
                        (awk-mode . "awk")
                        (other . "linux")))
