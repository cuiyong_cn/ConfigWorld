function forget_month_old {
    find . -type d -mtime +30 -exec sh -c 'echo $1' sh {} \;
}

function mark_it_old {
    local path=$1
    if [ -e "$path" ]; then
        touch -m -d "2024-01-01 00:00:00" $path
    else
        echo "$path does not exist"
    fi
}

function unpack {
    if [ -f "$1" ]; then
        case "$1" in
            *.tar.bz2) tar xjf "$1" ;;
            *.tar.gz)  tar xzf "$1" ;;
            *.bz2)     bunzip2 "$1" ;;
            *.rar)     unrar x "$1" ;;
            *.gz)      gunzip "$1" ;;
            *.tar)     tar xf "$1" ;;
            *.tbz2)    tar xjf "$1" ;;
            *.tgz)     tar xzf "$1" ;;
            *.zip)     unzip "$1" ;;
            *.Z)       uncompress "$1" ;;
            *.7z)      7z x "$1" ;;
            *) echo "Do not know how to unpack '$1'" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function find_file {
    find . -name "*$1*"
}

function file_contains {
    grep -rn "$1" .
}
