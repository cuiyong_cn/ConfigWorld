# Permission denied (Public Key)
```txt
Openssh start to deprecate SSH-RSA starting 8.2 onward, see https://www.openssh.com/txt/release-8.2

Work around, add below content in ~/.ssh/config
    Host *
        HostkeyAlgorithms +ssh-rsa
        PubkeyAcceptedKeyTypes +ssh-rsa
```

# Check Who Set the Value At Last
```txt
git config --show-origin <key>
```
