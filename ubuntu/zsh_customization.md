# PS1 customization

```sh
export PS1="%F{green}%4d"$'\n'"%f"

function settitle() {
    echo -n "\e]2;"$1"\007"
}

# if not in unix, delete the cygpath part
function chpwd() {
    settitle $(cygpath -u $(pwd))
}
```
