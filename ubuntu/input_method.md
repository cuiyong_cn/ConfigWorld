# Adding Chinese Input Method.
```text
1. Settings -> Region & Language -> Manage Installed Language -> Install/Remove languages -> Chinese
2. At the same time make sure the input method system is IBus
3. Reboot
4. Settings -> Keyboard -> Input Sources -> Select Chinese (Intelligent Pinyin)
```
