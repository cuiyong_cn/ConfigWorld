# static ip config
```shell
- ubuntu 18.04 onward using netplan

add yaml file at /etc/netplan/xxx.yaml with below content.

network:
  version: 2
  renderer: networkd
  ethernets:
    <eth interface name>:
      dhcp4: no
      dhcp6: no
      addresses: [xxx.xxx.xxx.xxx/xx, <ipv6 addr if any>]
      gateway4: xxx.xxx.xxx.xxx
      nameservers:
        addresses: [xxx.xxx.xxx.xxx,xxx.xxx.xxx.xxx]

- apply the changes
sudo netplan apply


- ubuntu 12.04 - 17.04
append below content in /etc/netwoek/interfaces

auto <eth interface name>
    | intf logical name | family | method |
iface <eth interface name> [inet|inet6|ipx|] [static|dhcp]
  address xxx.xxx.xxx.xxx
  netmask xxx.xxx.xxx.xxx
  gateway xxx.xxx.xxx.xxxc
  dns-nameservers xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx

- apply the changes
sudo systemctl restart ifup@<eth interface name>
```
--------------------------------------------------------------------------------

# How to Restart Network?

```shell
- GUI Method
Just turn off and turn on

- netplan
sudo netplan apply

- systemctl
sudo systemctl restart NetworkManager.service

- service
sudo service network-manager restart

- nmcli
sudo nmcli networking off
sudo nmcli networking on

- ifup & ifdown
sudo ifdown -a
sudo ifup -a

NOTE: this method only restart network interfaces marked as auto
```

--------------------------------------------------------------------------------

## Check network link speed
```shell
1. using ip utility
sudo ip -a | grep qlen

2. Query system file
sudo cat /sys/classes/net/<dev_name>/speed

3. ethtool command
sudo ethtool <interface_name>
```

--------------------------------------------------------------------------------

# Find which NIC is associated with the interface

```shell
- execute below command, and look behind your computer to see which NIC is blinking
ethtool -p <interface_name>
```

--------------------------------------------------------------------------------

# Troubleshoot Network

```shell
- first try to pint some host

- check routes
    ip route show

- ping the gateway to check if we can reach it

- ping widely accessible DNS server like 8.8.8.8

- if still can not access the internet, probably hostname to address resolution is not working

- use host or dig command to see if the DNS server is working
    host <remote_host> <dns_server ip>
    dig<remote_host> <dns_server ip>

- if connection is slow, use below command to check the hops
    traceroute <remote_hos>
```

--------------------------------------------------------------------------------

# /etc/resolve.conf reset when reboot
```txt
After upgrading ubuntu from 16.04 to 18.04, the network stopped working.
Immediately we found out this is a dns issue.

There should be a line "search <domain>" inside /etc/resolve.conf, but there isn't.

ubuntu 18.04 should use systemd-resolved stub resolver instead of dnsmasq.

At last we found /etc/NetworkManager/NetworkManager.conf configured dns=dnsmasq
Deleting it fixed the issue
```

--------------------------------------------------------------------------------

# Restart DNS resolver
```text
sudo systemctl restart systemd-resolved
```

--------------------------------------------------------------------------------

# A Start Job Ruuning for Wait for Network to be Configured blocks for 2 minutes

```text
More details see https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/2036358

sudo systemctl disable --now systemd-networkd.service

This will disable systemd-networkd and associated units, including
systemd-networkd-wait-online.service. NetworkManager and systemd-networkd should not be running at
the same time. On desktop, NetworkManager is the default network stack.
```

--------------------------------------------------------------------------------

# Wifi Adaptor not Found Message Appearing in Wifi setting

```text
Switch the netplan renderer from networkd to NetworkManager
```

# Slow SSH Login for a specific user
```
1 - This issue probably only happens on Debian distribution (e.g Ubuntu)
2 - And only some user have this kind of issue, others are fine.
Then most likely cause is: the user's .ssh folder's access attribute probably not 700.
sudo chmod 700 .ssh -- probably gonna fix it
```
