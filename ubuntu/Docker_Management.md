# docker install
please refer to docker official site

# docker storage location change
```text
normally, docker images resided in /var/lib/docker and it usually mounted in / filesystem which might be short on disk space

so we can mv all contents inside /var/lib/docker to another folder and create a soft link to the new location

ln -s <new> <old>
```

# docker group manage
docker usage need user in docker group.

```shell
sudo usermod -aG docker <user>
```

# move docker image from one host to another
```shell
1. docker save -o <tar-file> <image>
2. transfer the file to remote host
3. docker load -i <tar-file>

example:
    docker image list
    docker save -o my_image.tar repo:tag
    docker load -i my_image.tar
```
