# Order in which the shell checks for the commands you type

1. Aliases
2. Shell reserved word
3. Function
4. Built-in command
5. Filesystem command
