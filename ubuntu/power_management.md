# aer: corrected error received
```text
1. Add below parameters to limux boot command in /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pcie_aspm=off"

2. sudo update-grub
3. reboot to make it effect
```

# Pop noise issue
```text
Two solutions here:
    1. Modify /etc/pulse/default.pa (backup it first)
        comment out "load-module module-suspend-on-idle"

    2. Disable sound card's power saving mode
        2.1 check if it is on by "cat /sys/module/snd_hda_intel/parameters/power_save"
        2.2 if it is 1, it means power save mode is on
        2.3 disable it by "echo "0" | sudo tee /sys/module/snd_hda_intel/parameters/power_save"
        2.4 if above steps worked, then persist the configuration
            echo "options snd_hda_intel power_save=0" | sudo tee -a /etc/modprobe.d/audio_disable_powersave.conf
```

# Wake from Sleep by USB Devices
```text
1. Check if enabled already
    cat /sys/bus/usb/devices/usb1/power/wakeup
    cat /sys/bus/usb/devices/usb2/power/wakeup

2. Enable it
    echo enabled | sudo tee /sys/bus/usb/devices/usb1/power/wakeup
    echo enabled | sudo tee /sys/bus/usb/devices/usb2/power/wakeup

If we want to enable a specific devices, maybe we
need to check the devices by lshw utility
```

# Shutdown Stuck Like forever
```text
1. First we could press ESC to see what console output
2. If the shutdown stucked by "systemd-shutdown: waiting for process xxx", then check if xxx has
   some issues with it.
```
