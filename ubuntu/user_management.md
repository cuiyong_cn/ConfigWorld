# How To Add An New User

```shell
- Use any of the following command
adduser {username}

useradd -s /path/to/shell -d /home/{dirname} -m -G {secondary-group} {username}
passwd {username}
```

-------------------------

# How To Delete An User

```shell
- lock the user account
    passwd -l {username}

- kill all running processes of the user
    ps -u {username}
    killall -9 -u {username}

- back up all the user data (optional)
- delete user account and files
    userdel -r {username}

- verify
    id {username}
```

-------------------------

# How to Rename User Login Name?

```shell
usermod -l <NewName> <OldName>

We also need to manually change the home folder to reflect user name change

usermod -m -d /home/<NewName> <NewName>
```

-------------------------

# How to Rename User Group Name?

```shell
groupmod -n <newname> <oldname>
```

-------------------------

# User Sudo Privilege Management

```shell
- check if use has sudo privilege 
sudo -l -U <userbame>

- grant user sudo privilege
sudo usermod -aG sudo <username>

- cancel user sudo privilege
sudo deluser <username> sudo
```

-------------------------

# Check User's Last Login Date 

```shell
# last utility only show result recorded in /var/log/wtmp.# file
last <user name>

lastlog -u <user name>
```

-------------------------

# How to Restrict Disk Usage Per User?

```shell
we can use fixed sized image as a container for users home dir, and mount as loop dev

mkdir /media/users/
dd if=/dev/zero of=/media/users/foo.img bs=512K count=<amount>
mkfs.ext4 /media/users/foo.img
mkdir /home/foo
mount -o loop /media/users/foo.img /home/foo

then we enable automatic mount

/media/users/foo.img /home/foo ext4 loop 0 2
```

# Add a user to another group
```shell
usermod -aG <groupname> <username>
```

# Remove a user from a group
```shell
deluser <username> <groupname>
```

# Troubleshoot AD Account Constanly Locked Out Issue
```txt
1. Check the auth log to see if any auth failure
    sudo less /var/log/auth.log
```

# List all users belong to specified group
```txt
getent group <groupname>
```
