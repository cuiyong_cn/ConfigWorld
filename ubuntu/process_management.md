# How to Check What libs a Process Loaded
```
cat /proc/<pid>/maps | awk '{print $6}' | grep '\.so' | sort | uniq

or

lsof -p <pid> | grep '\.so'
```

# Check How Much Memory a Process Used
```
1 - Get pid
ps -ef | grep <process name>
2 - Check Memory Usage
cat /proc/<pid>/status | grep VmSize
```

# How to Check What libs a Binary Needed?
```
objdump -p <path to bin> | grep NEEDED
```
