# How to create a new group
groupadd <GroupName>

# How to delete a group
groupdel <GroupName>

# Add new user to a group
usermod -a -G <groupName> <userName>

------------------------------

# How to Change Group Name?

sudo usermod -n <NewGroupName> <OldGroupName>
