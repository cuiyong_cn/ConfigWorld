# How to Grant User Access?

sudo smbpasswd -a <username>

------------------------------

# How to Reload the Config?

------------------------------

sudo smbcontrol smbd reload-config

------------------------------

# How to List All Samba Users?

sudo pdbedit -L

------------------------------

# How to Delete Samba User?

sudo pdbedit -u <UserName> -x
