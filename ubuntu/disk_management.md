# How to Add New Disk Using Lvm?

```sh
// List device attached
fdisk -l
blkid
lshw -C disk

// Format disk
fdisk /dev/sd#
fdisk maybe too old, it can only partition upto 2TB.

parted /dev/sd#
mklabel gpt
mkpart primary 0% 100%
mkfs.ext4 /dev/sd#

// Delete a Partition
rm [part number]

// Verify the Partition
fdisk -l

// Logical Volume Management
pvcreate /dev/sd##
vgcreate <vg name> /dev/sd## /dev/sd##
vgdisplay

lvcreate -n <name> -l 100%FREE vgname
lvdisplay

mkfs.ext4 /dev/vgname/logicname

// Add below content in /etc/fstab
/dev/vgname/logicname /dir ext4 noatime 0 2

// we better use uuid instead of device name, because device name maybe different on every reboot
UUID=<uuid> <mount dir> <filesystem type> <mount options> 0

// How to Find out UUID
ls -lt /dev/disk/by-uuid
blkid [device]

// Do the following command, and it is good if no error shows up
sudo mount -a
```

------------------------------

# How to Delete a Soft Raid?

```sh
// Check Soft Raid Info
mdadm -D /dev/mdxx

// Unmount the Node
df -hTP
umount /xxx

// Mark All Underlying Device as Fail
mdadm /dev/mdxx --fail /dev/sd# --remove /dev/sd#

// Stop the Raid
mdadm --stop /dev/md##
mdadm --remove /dev/md##

// Clear All the Meta Info
mdadm --misc --zero-superblock /dev/sd#

// Check Raid Config and Delete Unwanted Info
/etc/mdadm/mdadm.conf
/etc/raidtab
/etc/rc.sysinit
```

------------------------------
# Replace Failed Mirror Disk in Soft RAID (mdadm)
```txt
1. Mark Disk as Failed
   sudo mdadm --manage /dev/<md device> --fail /dev/<device>

2. Verify
   cat /proc/mdstat

3. Remove Disk
   sudo mdadm --manage /dev/<md device> --remove /dev/<device>

4. Replace Disk Phisically

5. Add New Disk
   sudo mdadm --manage /dev/<md devixe> --add /dev/<device>

6. Verify
   sudo mdadm --detail /dev/<md device>

7. Check Recovering Process
   cat /proc/mdstat
```

------------------------------

# How to Create Soft Raid 0 Using mdadm?

```sh
// Raid 0 prerequites

1. at least 2 storage devices (same size, same brand, same model)
2. mdadm installed

// List available disk. Those type is disk with no mount point are what we looking for
lsblk

// Creating the Array
sufo mdadm --create --verbose /dev/md0 --level=0 --raid-devices=<disk count> /dev/sda /dev/sdb ...

// Make Sure Raid Created Successfully
cat /proc/mdstat

// Create and Mount Filesystem
sudo mkfs.ext4 /dev/md0
sudo mount /dev/md0 <mount point>

// Verify
df -h

// Saving Array Layout
sudo mdadm --detail --scan | sudo tee -a /etc/mdadm/mdadm.conf

// Update initramfs, so array be available during early boot process
sudo update-initramfs -u

// Add Below Content in /etc/fstab
/dev/md0 <moint point> ext4 defaults,nofailb0 0

// Other Raid Type Creation is Similar
```

------------------------------

# How to eject usb drive?

```shell
// device path can be found using lshw -short
sudo eject <device path>
```

------------------------------

# How to check which process holding open a device?
```shell
lsof <directory>
```

------------------------------

# Verify fstab Config
```txt
sudo findmnt --verify
```
