# 403 Forbidden
```txt
1. Check Nginx Error Logs
    sudo tail /var/log/nginx/error.log
2. Troubleshoot According to the Error Message

Possible Reason:
1. The folder you are visiting does not contain index.html but autoindex is off
```
