# GUI on boot

```shell
- Disable GUI on boot
sudo systemctl set-default multi-user

- Enable GUI on boot
sudo systemctl set-default graphical

- Start GUI Mannually (if it is gnome GUI)
sudo systemctl start gdm3
```

# Install the gui on ubuntu server
```shell
sudo apt-get install ubuntu-desktop

-- after installation finish. start the display manager, assume the manager is lightdm

sudo service start lightdm
```

# Create a Application Shortcut
```
1 - Create a file named "<YouAppName>.desktop" under ~/Desktop
2 - File content as below
[Desktop Entry]
Name=<YouAppName>
Comment=<Description About the App>
Exec=<Path to the executable of the App>
Terminal=false
Icon=<Paht to the icon of the App>
Type=Application
Categories=Development;
3 - Now switch to your desktop window. You should see an item named <YouAppName>
4 - Right click the item and select "Allow launching"
5 - Everything done.
```
