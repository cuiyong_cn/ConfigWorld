# How to Build Python?

```sh
sudo apt-get install build-essential checkinstall libreadline-gplv2-dev libffi-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev wget

wget https://www.python.org/ftp/python

tar xvf <python_tarzip_file>

./configure --prefix=<where_to_install> --enable-shared LDFLAGS="-Wl,-rpath <where_to_find_shared_lib>"

make
make install

or just check https://www.build-python-from-source.com/

``://www.python.org/ftp/python/3.11.1/Python-3.11.1.tgz
tar xzf Python-3.11.1.tgz
cd Python-3.11.1

sudo ./configure --prefix=/opt/python/3.11.1/ --enable-optimizations --with-lto --with-computed-gotos --with-system-ffi --enable-shared
sudo make -j "$(nproc)"
sudo make altinstall
sudo rm /tmp/Python-3.11.1.tgz

sudo /opt/python/3.11.1/bin/python3.11 -m pip install --upgrade pip setuptools wheel

sudo ln -s /opt/python/3.11.1/bin/python3.11        /opt/python/3.11.1/bin/python3
sudo ln -s /opt/python/3.11.1/bin/python3.11        /opt/python/3.11.1/bin/python
sudo ln -s /opt/python/3.11.1/bin/pip3.11           /opt/python/3.11.1/bin/pip3
sudo ln -s /opt/python/3.11.1/bin/pip3.11           /opt/python/3.11.1/bin/pip
sudo ln -s /opt/python/3.11.1/bin/pydoc3.11         /opt/python/3.11.1/bin/pydoc
sudo ln -s /opt/python/3.11.1/bin/idle3.11          /opt/python/3.11.1/bin/idle
sudo ln -s /opt/python/3.11.1/bin/python3.11-config      /opt/python/3.11.1/bin/python-config`

------------------------------

# How to Build Cmake?

```
wget https://github.com/Kitware/CMake/releases/download/

./bootstrap --prefix=<where_to_install>
make
make install

or an older cmake exists
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=<where_to_install> ..
make
make install
```

------------------------------

# How to Build Vim?

```
sudo apt-get install ruby-dev
sudo apt-get install libperl5.xx-dev
sudo apt-get install liblua5.3-dev
sudo apt-get install python3-dev

git clone https://github.com/vim/vim.git

./configure --prefix=<where_to_install> --with-features=huge --enable-multibyte --enable-rubyinterp --enable-luainterp --enabke-perlinterp --enable-python3interp --with-python3-config-dir=<your_self_build_python_config>
```

------------------------------

# How to Build Clang?

```
-- Using Makefile
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=<where_to_install> -DBUILD_SHARED_LIBS=On -DLLVM_TARGETS_TO_BUILD=X86 -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra" -DLLVM_INCLUDE_EXAMPLES=OFF -DLLVM_INCLUDE_TESTS=OFF ../llvm

-- Using Ninja
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS=clang -B build -S llvm
cmake --build build -j2 --target check-all
cmak --install build
```
