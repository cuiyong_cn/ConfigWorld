# how to check ubuntu version

```shell
- Using any of the below command

lsb_release -a

cat /etc/issue

cat /etc/os-release

hostnamectl
```

# Check system serial number

```shell
sudo dmidecode -t system | grep Serial
```

# How to change hostname without restart

```shell
- Temporarily change hostname

sudo hostname <new host name>

- Persist the change

modify below two files
/etc/hostname
/etc/hosts

then restart the hostname service

sudo systemctl restart systemd-logind.service

- We can also accomplish this via hostnamectl

hostnamectl set-hostname <new host name>

then might need to add auth cookie

sudo xauth list
sudo xauth add "<new host name>/unix:0" MIT-MAGIC-COOKIE-1 <cookie-id>
```

# How to fix error: /boot;vmlinuz-x.x.x-xxx-generic.efi.signed has invalid signature?
This error means that, the new kernel can not pass the security check.

We only need to disable the security boot in the config of motherboard.

# Check Uptime and Boot Time
```shell
- up time
uptime -p

- boot time
uptime -s
```

# Check Mac address
```
    ifconfig -a | grep HWaddr
    cat /sys/class/net/<interface>/address
```

# Upgrade OS
```shell
Caution: Please make sure it host have enough free space in root dir (at least 20GB)

1. Upgrade current software
2. do-release-upgrade

do-release-upgrade config file:
/etc/update-manager/meta-release
/etc/update-manager/release-upgrades
Issue:
    when do-release-upgrade, it report no new
    release found.

    export DEBUG_UPDATE_MANAGER=true
    do-release-upgrade -c will output more detailed error.
    And we found ssl verify failed.

    Then we found openssl link against with wrong lib, it is installed by
    others, and it is not the official lib.

    ldd /usr/bin/openssl
    Check diffs between normal host and problemic host.

    And remove the wrong lib fixed that.

    Most of the time, it is related to libcryto.so or libssl.so lib.

--------------------------------------------------------------------------------
    do-release-upgrade not found.
    sudo apt-get install ubuntu-release-upgrader-core gonna fix it
```

# How to Increase Virtual Memory
```txt
1. Check Current Swap Space
    sudo swapon --show or free -h
2. Create a Swap File
    sudo fallocate -l xxG /path/to/swapfile
    sudo chmod 600 /path/to/swapfile
3. Make the File as Swap Space
    sudo mkswap /path/to/swapfile
    sudo swapon /path/to/swapfile
4. Add Below Contents in /etc/fstab to Persist the Settings
    /path/to/swapfile none swap sw 0 0
```

# How to Prevent System from Going to Sleep
```txt
1. mask sleep target
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

2. unmask sleep target
sudo systemctl unmask sleep.target suspend.target hibernate.target hybrid-sleep.target

```

# Display System Info
```
sudo dmidecode -t 1,4
```
