# How to set PATH var globally
'''shell
put the env set script under /etc/profile.d/
'''

# Change sh to bash instead of dash
```
sudo dkpg-reconfigure dash
```

# How to Change Timezone
```
1 - Check current timezone
    timedatextl
2 - List available timezones
    ls -l /etc/share/zoneinfo
 or timedatectl list-timezones
3 - sudo timedatectl set-timezone <region/city>
4 - Verify
    timedatectl
```

# How to config library finding path
```
Option 1
    Set variable LD_LIBRARY_PATH=<colon separated list>

    This only affects one binary. You can write a wrapper scrit.

Option 2
    If /etc/ld.so.conf contains "include /etc/ld.so.conf.d/*.conf"
    Then you can add your own path in a file and put it inside that folder.
    Last, execute /sbin/ldconfig to make it effective

Option 3
    You can compile the bin with option -Wl,-R<lib path you want>
```

# How to Remove Keyring Added by apt-key add
```script
1 - First check current keys
    apt-key list

output something like:
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
/etc/apt/trusted.gpg.d/ubuntu-keyring-2012-cdimage.gpg
------------------------------------------------------
pub   rsa4096 2012-05-11 [SC]
      8439 38DF 228D 22F7 B374  2BC0 D94A A3F0 EFE2 1092 ---> this is the key
uid           [ unknown] Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>

/etc/apt/trusted.gpg.d/ubuntu-keyring-2018-archive.gpg
------------------------------------------------------
pub   rsa4096 2018-09-17 [SC]
      F6EC B376 2474 EDA9 D21B  7022 8719 20D1 991B C93C
uid           [ unknown] Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>


2 - Find the key you want to remove and then remove it.
    sudo apt-key del <key-id>
```
