# What's up with "undecodable token: \001b(hex)[?2004h"? 

It turns out tha libreadline from version 8 onward, default enabled the
bracket paste mode, and causing this issue. So add below contents in
~/.inputrc file will make this annoying output disappear.

```inputrc
$if Octave
    set enable-bracketed-paste off
$endif
```
