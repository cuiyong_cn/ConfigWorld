# Update GCC
```shell
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update
sudo apt-get install gcc gcc-x g++-x

# configure alternatives
Now we have different versions of gcc, when we run gcc,
it will still be the previous one. Do the following

1. remove all previous alternatives
sudo update-alternatives --remove-all gcc

2. configure other alternatives
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 90 --slave /usr/bin/g++ g++ /usr/bin/g++-5
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 80 --slave /usr/bin/g++ g++ /usr/bin/g++-6
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 --slave /usr/bin/g++ g++ /usr/bin/g++-7

3. finally set the default one
sudo update-alternatives --config gcc
```

# Update Python
``` shell
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install pythonx.x

make the specified version of python be default one

sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.x [weight]
```

# bash: No such file or directory
```
Bash report above message when executing binary

1. check runtime linker
    readlelf --all exefile | grep interpreter
    ldd exefile

you probably see [Requesting program interpreter: /lib64/ld-lsb-x86-64.so.3]

2. try install lsb package
    apt-get install lsb
```

# No module named `apt_pkg`
```
Cause:
    every ubuntu release has its own default    python version.
    but sometimes we need a non-default version.
    so we install it by using deadsnake repo.
    this is where things get messy.
    python-apt/python3-apt is system package, it's for default python. but python scripts always using the currently configured versio. so it can not find the apt package.

Solution:
    cd /usr/lib/python3/dist-packages
    sudo ln -s apt_pkg.cpython-{version}-x86_64-linux-gnu.so apt_pkg.so
```

# Docker Installation
```shell
Follow instruction on docker official site.

Remember to add relevent user to docker group
```

# Module mako not found
'''txt
sudo apt install python-mako
'''

# xgettext not found
```txt
sudo apt install gettext
```

# Minimized Ubuntu do not have manpages
```text
1. Minimized Ubuntu makes some rules in /etc/dpkg/dpkg.cfg.d/excludes, which exclude man and manpages from being installed.
2. It also switches the man binary to be a script that oututs messages like this is minimized Ubuntu. Tell you to unminimize it.

We can comment out following line
path-exclude=/usr/share/man

3. Then reinstall man, manpages, man-db, mandoc, dialog.
4. Rename man.REAL to man and man pages will be back
```

# How to List Config Files of Installed Software
```text
cat /var/lib/dpkg/info/<package_name>.conffiles
```

# libffi6.so for Ubuntu 20.04
```txt
Ubuntu 22.04 upgraded libffi6 to libffi7, so if your app use libffi6, 2 options available

1 - Just create a symlink to the new veraion
    sudo ln -s /usr/lib/x86_64-linux-gnu/libffi.so.7 /usr/lib/x86_64-linux-gnu/linffi.so.6

    Some people might strongly against this solution, but this one actually is safe, because libffi interfaces are
    backward compatible and quite stable.    You can check the source code to confirm this.

2 - You can find a libffi6 deb package and install, but I don't like this one, because now you have 2 different version,
and may cause some other unseen errors.
```

# List all installed packages
```txt
apt list --installed
```
