# why we need this?
ubuntu official app repository
usually not provide latest version of app. But we need the new features of the latest version app.

# How to upgrade the app to latest?
key idea is, find a non-official but trusted app repository source.

Below are often used app source:

## git
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git
