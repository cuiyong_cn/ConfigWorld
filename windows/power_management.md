# Find out what devices capable of waking pc up from standby
```
powercfg -devicequery wake_from_any
```

# Find out what devices can actually waking pc up from standby
```
powercfg -devicequery wake_armed
```

# Disable the device which can wake pc up from standby
```
powercfg -devicedisablewake "device name"
```

# Monitor or display is still black when waking up from sleep.
```txt
probably graphics driver issue, try restart driver using below CMD
    Ctr + Shift + Win + B
```

# Reboot system
```txt
In command prompt, issue below command
    shutdown /r
```
