# How to Concatenate Multiple Video Files into One?
```txt
1. First we create a file with contents like below

file 'file1'
file 'file2'
file 'file3'
file 'file4'

2. then execute below command to get the job done
ffmpeg -f concat -safe 0 -i <concate_file> -c copy <new_video_name>
```
