# Check netword link speed
```powershell
Get-NetAdapter
```

# Flush dns cache
```cmd
ipconfig /flushdns
```
