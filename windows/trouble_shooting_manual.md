# Start Menu won't run

```
1. Try to delete all files under C:\Windows\System32\AppLocker except AppCache.dat.
2. If the folder already empty in step 1. Try below:
   1. Open regedit
   2. Go to HKEY_CURRENT_USER\Software\Windows\CurrentVersion\Explorer\User Shell Folders
   3. Right click, select "Permissions"
   4. Click "Advanced" button
   5. Tick "Replace all child object..." at left bottom corner
   6. Click "Enable inheritance" button
   7. Click "Ok" button
   8. Restart PC, check if issue resolved
```
