# make system partition writable
emulator -avd <AVDNAME> -writable-system

Note: right now, if we want to persist the changes
in system, we need to keep using -writable-system
to start the avd


# to list all avd
avdmanager list avd

# install app into /system/priv-app
1. df -h
2. mount -o remount,rw <system partition>
3. adb push apk <system partition>
4. reboot the device
