# List Installed Packges
'''shell
pm list packages
'''

# Find the Launchable Activity
'''shell
Any of the below command should do the trick

dumpsys package <packagename> | grep -A 1 MAIN
pm dump <packagename> | grep -A 1 MAIN

pm list packages -f
adb pull <apk path from previous cmd>
aapt dump badging <apk file>
'''

# Start an Activity
```text
adb shell am start -n "com.example.greatingcard/com.example.greatingcard.MainActivity" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
```

# List All Binderized Services
```
adb shell lshal
```
