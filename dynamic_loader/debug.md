# Specifying library search paths in an object
- Use `LD_LIBRARY_PATH=<path>`
- Installing library in one of the standard directories
- During static linking, insert a list of directories into the executable
  - -Wl,-rpath,`<path1>` -Wl,-rpath,`<path2>`
  - -Wl,-rpath,`<path1>:<path2>`

note: Shared libraries can have rpath lists

# Dynamic string tokens
DL understands certain special strings in rpath list

$ORIGIN: expands to directory containing program or library
$LIB: expands to lib or lib64, depending on architecture
$PLATFORM: expands to string corresponding to processor type

note:
  DL also understands these names in some other contexts
  like `LD_LIBRARY_PATH, LD_PRELOAD, LD_AUDIT, and dlopen()`

# Finding shared libraries at run time
When resolving dependencies in dynamic dependency list,
DL deals with each dependency string as follows:
  - If the string contains a slash >> interpret dependency as a relative or absolute pathname
  - Otherwise, search for shared library using these rules
    - If calling object has `DT_RPATH` list and does not have `DT_RUNPATH`list,
      search directories in `DT_RPATH` list
    - If `LD_LIBRARY_PATH` defined, search directories it specifies. For security reasons,
      `LD_LIBRARY_PATH` is ignored in “secure” mode (set-UID and set-GID programs, etc.
    - If calling object has `DT_RUNPATH` list, search directories in that list
    - Check /etc/ld.so.cache for a corresponding entry
    - Search /lib and /usr/lib (in that order) or /lib64 and /usr/lib64

# Run-time symbol resolution
When a symbol definition inside an object is overridden by
an outside definition, we say symbol has been interposed
  - Interposition can occur for both functions and variables
Surprising, but good historical reason for this behavior
Shared libraries are designed to mirror traditional static library semantics:
  - Definition of global symbol in main program overrides version in library
  - Surprising, but good historical reason for this behavior
  - Shared libraries are designed to mirror traditional static library semantics:
    - Definition of global symbol in main program overrides version in library
    - Global symbol appears in multiple libraries?
      reference is resolved to first definition when scanning libraries in left-to-right order
      as specified in static link command line
  - Interposition behavior made transition from static to shared libraries easier

# Interposition vs libraries as self-contained subsystems
Symbol interposition semantics conflict with model of shared library as a self-contained subsystem
  - Shared library can’t guarantee that reference to its own global symbols will bind to those symbols at run time
  - Properties of shared library may change when it is aggregated into larger system
Can sometimes be desirable to force symbol references within a shared library to resolve to library’s own symbols

`-Bsymbolic` linker option causes references to global symbols within shared library to resolve
to library’s own symbols

gcc --shared -Wl,-Bsymbolic -o libfoo.so foo.o

downside is: Affects all symbols in shared library!
good news is: Other techniques can provide this behavior on a per-symbol basis

# Symbol resolution and library load order
Dependent libraries are added in breadth-first order.
Symbols are resolved by searching libraries in load order.

# The `LD_DEBUG` environment variable
`LD_DEBUG` can be used to monitor operation of dynamic linker

Valid options for the `LD_DEBUG` environment variable are:
  libs        display library search paths
  reloc       display relocation processing
  files       display progress for input file
  symbols     display symbol table processing
  bindings    display information about symbol binding
  versions    display version dependencies
  scopes      display scope information
  all         all previous options combined
  statistics  display relocation statistics
  unused      determined unused DSOs
  help        display this help message and exit
